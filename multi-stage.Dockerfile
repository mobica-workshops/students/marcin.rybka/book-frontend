FROM node:18 AS builder
WORKDIR /build
ADD . ./
COPY secrets/local.env.json public/config/local.env.json
RUN ./build.sh

FROM nginx:alpine
RUN mkdir /app
COPY --from=builder /build/dist /app
COPY nginx.conf /etc/nginx/nginx.conf